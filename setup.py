# coding: utf-8
"""
    Flywheel BIDS Client
"""
import os

from setuptools import setup, find_packages

NAME = "flywheel-bids"
VERSION = os.getenv("CI_COMMIT_TAG", "0")
REQUIRES = [
    "flywheel-sdk>=14.6.3",
    "future>=0.18.2",
    "jsonschema>=3.2.0",
    "rtstatlib>=1.0.0",
    "urllib3>=1.26.4",
]

setup(
    name=NAME,
    version=VERSION,
    description="Flywheel BIDS Client",
    author_email="support@flywheel.io",
    url="",
    keywords=["Flywheel", "flywheel", "BIDS", "SDK"],
    install_requires=REQUIRES,
    packages=find_packages(),
    include_package_data=True,
    package_data={"flywheel_bids": ["templates/*.json"]},
    license="MIT",
    project_urls={"Source": "https://github.com/flywheel-io/bids-client"},
    entry_points={
        "console_scripts": [
            "curate_bids=flywheel_bids.curate_bids:main",
            "export_bids=flywheel_bids.export_bids:main",
            "upload_bids=flywheel_bids.upload_bids:main",
        ]
    },
    long_description="""
Flywheel BIDS Client
====================

An SDK for interacting with BIDS formatted data on a Flywheel instance.
""",
)
