# bids-client

FROM python:3.9-slim

MAINTAINER Flywheel <support@flywheel.io>

RUN apt-get update && apt-get install -y nodejs npm bash jq && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# https://www.npmjs.com/package/bids-validator
RUN npm install -g bids-validator@1.8.4

COPY requirements.txt /var/flywheel/code/requirements.txt
RUN pip install -r /var/flywheel/code/requirements.txt

RUN rm -rf /var/cache/apk/*

COPY . /var/flywheel/code/bids-client
RUN pip install --no-deps /var/flywheel/code/bids-client
