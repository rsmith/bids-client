import logging
from unittest.mock import patch
import pytest

import flywheel

from flywheel_bids.supporting_files import BIDSCuration

log = logging.getLogger(__name__)

ACQUISITION_LABELS = {
    "Localizer": 76,
    "BOLD_taxi2": 73,
    "MPRAGE_P2_1mm": 75,
    "PhoenixZIPReport": 64,
    "B0map": 146,
    "BOLD_taxi3": 70,
    "BOLD_taxi4": 69,
    "BOLD_taxi1": 75,
    "BOLD_taxi5": 31,
    "BOLD_localizer2": 14,
    "BOLD_localizer1": 15,
    "BOLD_taxi6": 16,
}

SUBJECTS_HAVE = {
    "sub-01": {
        "Localizer": 6,
        "BOLD_taxi2": 5,
        "MPRAGE_P2_1mm": 6,
        "PhoenixZIPReport": 5,
        "B0map": 10,
        "BOLD_taxi3": 5,
        "BOLD_taxi4": 5,
        "BOLD_taxi1": 6,
        "BOLD_taxi5": 2,
        "BOLD_localizer2": 1,
        "BOLD_localizer1": 1,
        "BOLD_taxi6": 1,
    },
    "sub-02": {
        "PhoenixZIPReport": 5,
        "BOLD_taxi2": 5,
        "B0map": 10,
        "BOLD_taxi3": 5,
        "MPRAGE_P2_1mm": 5,
        "BOLD_taxi4": 5,
        "Localizer": 5,
        "BOLD_taxi5": 4,
        "BOLD_taxi1": 5,
        "BOLD_localizer1": 2,
        "BOLD_localizer2": 1,
        "BOLD_taxi6": 1,
    },
    "sub-03": {
        "BOLD_taxi2": 5,
        "BOLD_taxi3": 5,
        "BOLD_taxi1": 5,
        "PhoenixZIPReport": 4,
        "Localizer": 5,
        "B0map": 10,
        "BOLD_taxi5": 2,
        "BOLD_taxi4": 5,
        "MPRAGE_P2_1mm": 5,
        "BOLD_taxi6": 1,
        "BOLD_localizer2": 1,
        "BOLD_localizer1": 1,
    },
    "sub-14": {
        "Localizer": 6,
        "PhoenixZIPReport": 6,
        "MPRAGE_P2_1mm": 6,
        "BOLD_taxi1": 6,
        "B0map": 12,
        "BOLD_taxi2": 6,
        "BOLD_taxi3": 5,
        "BOLD_taxi4": 5,
        "BOLD_taxi5": 1,
        "BOLD_taxi6": 1,
        "BOLD_localizer1": 1,
        "BOLD_localizer2": 1,
    },
}


@patch("flywheel.Client")
def test_filter_intended_fors_exception(mock1, caplog):
    caplog.set_level(logging.DEBUG)
    fw = flywheel.Client("")
    bc = BIDSCuration.BIDSCuration()
    regexes = "one two three"
    with pytest.raises(Exception):
        new_intended_fors = BIDSCuration.filter_intended_fors(fw, bc, regexes)


@patch("flywheel.Client")
def test_filter_intended_fors_works(mock1):

    fw = flywheel.Client("")

    bc = BIDSCuration.BIDSCuration()
    bc.all_intended_fors["sub-001"] = {
        "ses-001": {
            "fmap-name": [
                "ses-001/func/sub-001_ses-001_task-bart_run-1_bold.nii.gz",
                "ses-001/func/sub-001_ses-001_task-bart_run-2_bold.nii.gz",
            ]
        }
    }
    bc.all_intended_for_acq_label["sub-001"] = {
        "ses-001": {"fmap-name": "acquisition-label"}
    }
    bc.all_intended_for_acq_id["sub-001"] = {
        "ses-001": {"fmap-name": "60d9f5b4cdd96dd507e924b6"}
    }

    regexes = "acquisition-label .*run-1.*"

    new_intended_fors = BIDSCuration.filter_intended_fors(fw, bc, regexes)

    assert (
        new_intended_fors["sub-001"]["ses-001"]["fmap-name"][0]
        == "ses-001/func/sub-001_ses-001_task-bart_run-1_bold.nii.gz"
    )


def test_get_most_subjects_count_works():
    most_subjects_have_count = BIDSCuration.get_most_subjects_count(
        ACQUISITION_LABELS, SUBJECTS_HAVE
    )
    assert most_subjects_have_count == {
        "Localizer": {6: 2, 5: 2},
        "BOLD_taxi2": {5: 3, 6: 1},
        "MPRAGE_P2_1mm": {6: 2, 5: 2},
        "PhoenixZIPReport": {5: 2, 4: 1, 6: 1},
        "B0map": {10: 3, 12: 1},
        "BOLD_taxi3": {5: 4},
        "BOLD_taxi4": {5: 4},
        "BOLD_taxi1": {6: 2, 5: 2},
        "BOLD_taxi5": {2: 2, 4: 1, 1: 1},
        "BOLD_localizer2": {1: 4},
        "BOLD_localizer1": {1: 3, 2: 1},
        "BOLD_taxi6": {1: 4},
    }


@patch("flywheel_bids.supporting_files.BIDSCuration.open")
@patch("flywheel_bids.supporting_files.BIDSCuration.csv")
def test_most_subjects_have_gets_set(mock1, mock2):
    bc = BIDSCuration.BIDSCuration()
    bc.acquisition_labels = ACQUISITION_LABELS
    bc.subjects_have = SUBJECTS_HAVE
    bc.save_acquisition_details(1, 2, "hunky_dory")
    assert bc.most_subjects_have == {
        "Localizer": [6, 5],
        "BOLD_taxi2": [5],
        "MPRAGE_P2_1mm": [6, 5],
        "PhoenixZIPReport": [5],
        "B0map": [10],
        "BOLD_taxi3": [5],
        "BOLD_taxi4": [5],
        "BOLD_taxi1": [6, 5],
        "BOLD_taxi5": [2],
        "BOLD_localizer2": [1],
        "BOLD_localizer1": [1],
        "BOLD_taxi6": [1],
    }
    bc.save_acquisitions("hunky_dory")
