# Dataset

The dataset consists of two subject, each with two sessions.

Includes: 
    - Main BIDS project-level files (e.g., dataset_description.json)
    - Variety of datatypes (anat, dwi, func)
    - Run indexing
Future:
    - Additional datatypes (fmap)
    - Physio & events
    - An IntendedFor

# Workflow

1. Run bids-validator with success (no errors) on the dataset.
2. Run the functional equivalent of fw import bids on the dataset.
3. Run the functional equivalent of curate-bids Gear on the imported dataset.
4. Run the functional equivalent of fw export bids on the imported dataset.
5. Run bids-validator on the exported dataset.
6. Run the functional equivalent of fw import bids on the exported dataset.
7. Run the functional equivalent of curate-bids Gear on the re-imported dataset.

# Requirements

You must have the appropriate Flywheel permissions to write across the Flywheel hierarchy.

# Recommended steps to run

1. Download the repository
2. Build the docker image
   
    `docker build -t bids-client:integration-test ./`
    
3. Run the docker image
   
    `docker run -it -v <PATH-TO-BIDS-CLIENT-DIRECTORY>:/flywheel/v0/bids-client --entrypoint /bin/bash bids-client:integration-test`
    
4. Inside the docker image
   
    `cd flywheel/v0/bids-client`
   
    `export FW_KEY=<FLYWHEEL-API-KEY>`
   
    `python3 tests/integration_bids_workflow.py`
   
When finished, you may proceed with the instructions in the log.
