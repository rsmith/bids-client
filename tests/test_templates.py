import json
import unittest

from flywheel_bids.supporting_files import templates


class RuleTestCases(unittest.TestCase):
    def test_rule_initialize_switch(self):
        rule = templates.Rule(
            {
                "template": "test",
                "where": {"x": True},
                "initialize": {
                    "Property": {
                        "$switch": {
                            "$on": "value",
                            "$cases": [
                                {"$eq": "foo", "$value": "found_foo"},
                                {"$eq": "bar", "$value": "found_bar"},
                                {"$default": True, "$value": "found_nothing"},
                            ],
                        }
                    }
                },
            }
        )

        context = {"value": "foo"}
        info = {}
        rule.initializeProperties(info, context)
        self.assertEqual(info, {"Property": "found_foo"})

        context = {"value": "bar"}
        info = {}
        rule.initializeProperties(info, context)
        self.assertEqual(info, {"Property": "found_bar"})

        context = {"value": "something_else"}
        info = {}
        rule.initializeProperties(info, context)
        self.assertEqual(info, {"Property": "found_nothing"})

    def test_rule_initialize_switch_lists(self):
        rule = templates.Rule(
            {
                "template": "test",
                "where": {"x": True},
                "initialize": {
                    "Property": {
                        "$switch": {
                            "$on": "value",
                            "$cases": [
                                {"$eq": ["a", "b", "c"], "$value": "match1"},
                                {"$eq": ["a", "d"], "$value": "match2"},
                                {"$default": True, "$value": "no_match"},
                            ],
                        }
                    }
                },
            }
        )

        context = {"value": ["c", "b", "a"]}
        info = {}
        rule.initializeProperties(info, context)
        self.assertEqual(info, {"Property": "match1"})

        context = {"value": ["a", "d"]}
        info = {}
        rule.initializeProperties(info, context)
        self.assertEqual(info, {"Property": "match2"})

        context = {"value": ["a", "b"]}
        info = {}
        rule.initializeProperties(info, context)
        self.assertEqual(info, {"Property": "no_match"})

        context = {"value": ["a", "b", "c", "d"]}
        info = {}
        rule.initializeProperties(info, context)
        self.assertEqual(info, {"Property": "no_match"})

    def test_rule_initialize_switch_regex(self):
        rule = templates.Rule(
            {
                "template": "test",
                "where": {"x": True},
                "initialize": {
                    "Property": {
                        "$switch": {
                            "$on": "value",
                            "$cases": [
                                {"$regex": "^[a-z]_[0-9]$", "$value": "match1"},
                                {"$regex": "^[0-9]_[A-Z]$", "$value": "match2"},
                                {"$default": True, "$value": "no_match"},
                            ],
                        }
                    }
                },
            }
        )

        context = {"value": "f_5"}
        info = {}
        rule.initializeProperties(info, context)
        self.assertEqual(info, {"Property": "match1"})

        context = {"value": "8_U"}
        info = {}
        rule.initializeProperties(info, context)
        self.assertEqual(info, {"Property": "match2"})

        context = {"value": "asdf"}
        info = {}
        rule.initializeProperties(info, context)
        self.assertEqual(info, {"Property": "no_match"})

    def test_rule_initialize_switch_neq(self):
        rule = templates.Rule(
            {
                "template": "test",
                "where": {"x": True},
                "initialize": {
                    "Property": {
                        "$switch": {
                            "$on": "value",
                            "$cases": [
                                {"$neq": "foo", "$value": "match1"},
                                {"$default": True, "$value": "no_match"},
                            ],
                        }
                    }
                },
            }
        )

        context = {"value": "bar"}
        info = {}
        rule.initializeProperties(info, context)
        self.assertEqual(info, {"Property": "match1"})

        context = {"value": "foo"}
        info = {}
        rule.initializeProperties(info, context)
        self.assertEqual(info, {"Property": "no_match"})

    def test_rule_initialize_format(self):
        rule = templates.Rule(
            {
                "template": "test",
                "where": {"x": True},
                "initialize": {
                    "Property": {
                        "value": {
                            "$take": True,
                            "$format": [
                                # Use regex to find patterns
                                {
                                    "$replace": {
                                        "$pattern": "[A-Z]+",
                                        "$replacement": "NEW",
                                    }
                                },
                                # Chain formatting operations
                                {"$lower": {"$pattern": "EW"}},
                            ],
                        }
                    }
                },
            }
        )
        rule2 = templates.Rule(
            {
                "template": "test",
                "where": {"x": True},
                "initialize": {
                    "Property": {
                        "value": {
                            "$take": True,
                            "$format": [
                                # Use regex to find patterns
                                {
                                    "$replace": {
                                        "$pattern": "[A-Z]+",
                                        "$replacement": "UPPER_12_key",
                                    }
                                },
                                # Chain formatting operations
                                {"$lower": True},
                            ],
                        }
                    }
                },
            }
        )
        context = {"value": "the_OLD_string"}
        info = {}
        rule.initializeProperties(info, context)
        self.assertEqual(info, {"Property": "the_New_string"})
        info = {}
        rule2.initializeProperties(info, context)
        self.assertEqual(info, {"Property": "the_upper_12_key_string"})

    def test_rule_where_regex_match(self):
        rule = templates.Rule({"template": "test", "where": {"x": {"$regex": "topup"}}})
        context = {"x": "string_with topup in it"}
        self.assertTrue(rule.test(context))

    def test_rule_in_string(self):
        """ """
        rule = templates.Rule(
            {"template": "test", "where": {"x.l": {"$in": ["topup", "something_else"]}}}
        )
        # Define context

        context = {"x": {"l": "string_with topup in it"}}
        # Call function
        self.assertTrue(rule.test(context))

        rule = templates.Rule(
            {
                "template": "test",
                "where": {"x": {"$in": ["not_topup", "something_else"]}},
            }
        )
        # Call function
        self.assertFalse(rule.test(context))

    def test_rule_where_not(self):
        """ """
        rule = templates.Rule(
            {"template": "test", "where": {"x": {"$not": {"$in": ["Value"]}}}}
        )
        context = {"x": "Value"}
        self.assertFalse(rule.test(context))
        context = {"x": "Something"}
        self.assertTrue(rule.test(context))

    def test_rule_where_or_and(self):
        """Nested conditions are a bit tricky. Note the list format for the $or clause.
        These tests work on the logic in template.py to sort out nested conditions using
        a simplified version of the diffusion rule. Neither inner $and criterion is satisfied
        in the third test, so the $or outer criterion should also return false.
        """
        rule = templates.Rule(
            {
                "template": "test",
                "where": {
                    "$or": [
                        {
                            "$and": {
                                "x": {"$in": ["JSON"]},
                                "y": {"$in": ["Diffusion"]},
                            }
                        },
                        {
                            "$and": {
                                "x": {"$in": ["nifti"]},
                                "y": {"$in": ["ORIGINAL"]},
                            }
                        },
                    ]
                },
            }
        )

        context = {
            "x": "JSON",
            "y": "Diffusion",
        }
        self.assertTrue(rule.test(context))

        context = {
            "x": "nifti",
            "y": "ORIGINAL",
        }
        self.assertTrue(rule.test(context))

        context = {
            "x": "nifti",
            "y": "Diffusion",
        }
        self.assertFalse(rule.test(context))

    def test_regex_bids_ignore_works(self):
        """ """
        with open("flywheel_bids/templates/default.json") as jfp:
            bids_v1_template = json.load(jfp)
            for rule in bids_v1_template["rules"]:
                print(rule["template"])
                if rule["template"] == "acquisition":
                    switchDef = rule["initialize"]["ignore"]["$switch"]

        context = {"acquisition": {"label": "3Plane_Loc_fgre_ignore-BIDS"}}
        assert templates.handle_switch_initializer(switchDef, context)
        context = {"acquisition": {"label": "3Plane_Loc_fgre"}}
        assert not templates.handle_switch_initializer(switchDef, context)
        context = {"acquisition": {"label": "ignore-BIDS"}}
        assert templates.handle_switch_initializer(switchDef, context)
        context = {"acquisition": {"label": "3Plane_Loc_fgre_ignore-BIDS_and_more"}}
        assert templates.handle_switch_initializer(switchDef, context)
        context = {"acquisition": {"label": "ignore_BIDS"}}
        assert not templates.handle_switch_initializer(switchDef, context)
