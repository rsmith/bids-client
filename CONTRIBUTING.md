# Contributor Guidelines

**bids-client** is a library that provides tooling for the BIDS Specification in Flywheel.

## Getting Started

***Clone the bids-client repo:***

```bash
$ git clone git@gitlab.com:flywheel-io/public/bids-client.git
$ cd bids-client
```

***Synchronize your master branch with the release-candidate branch:***

```bash
$ git fetch --all
$ git checkout release-candidate
```

***Create a feature branch to hold your development changes, using the GEAR-##-description-of-changes format:***

```bash
$ git checkout -b <GEAR-##-description-of-changes> release-candidate
```
and start making changes. Always use a feature branch from the release-candidate branch.

***Record your changes:***

```bash
$ git add <files-changed>
$ git commit -m "<ENH/DOC/BUG/MAIN: changes made.>"
```

***Push your changes to GitLab:***
```bash
$ git push origin <GEAR-##-descriptive-of-changes>
```
and create a merge request for your changes to the release-candidate branch. Note, all code review occurs on the release-candidate branch.

***After your MR has been approved, merge your changes to the release-candidate branch without a fast-forward:***
```bash
$ git checkout release-candidate
$ git merge --no-ff <GEAR-##-descriptive-of-changes> -m "<Merge branch GEAR-## with changes made into release-candidate.>"
$ git push origin release-candidate
```

***When the merge is complete and all pipelines are passing, delete your feature branch:***
```bash
$ git push -d origin <GEAR-##-descriptive-of-changes>
$ git branch -d <GEAR-##-descriptive-of-changes>
```

## Release of bids-client

When a set amount of work has been completed on the release-candidate branch, for example, all work for an EPIC has been completed, then the release-candidate branch is tagged. This tagged version is used for testing the downstream tooling, namely, the curate-bids Gear and the Flywheel CLI.

```bash
$ git checkout release-candidate
$ git tag -a <X.Y.Z-rc> -m "Release candidate version X.Y.Z-rc"
$ git push origin <X.Y.Z-rc>
```

When testing has been successfully completed on downstream tooling, merge the release-candidate branch to master and cut a release of bids-client.

```bash
$ git checkout master
$ git merge --no-ff release-candidate -m "Version X.Y.Z approved."
$ git push origin master
$ git tag -a <X.Y.Z> -m "Release version <X.Y.Z-rc>"
$ git push origin <X.Y.Z>
```

The master branch remains deployable and consistent with current tagged released used by the curate-bids Gear. All development work is performed on the release-candidate branch.


## Example Git Workflow
```
master
|
|\
| \
|  release-candidate
|  |
|  |\
|  | \
|  |  GEAR-42-fancy-feature
|  | /
|  |/
|  |\
|  | \
|  |  GEAR-11-bug-fix
|  | /
|  |/
|  |> tag rc version X.Y.Z >> test curate-bids Gear & Flywheel CLI
|  /
| /
|> cut release version X.Y.Z
|
```
